package configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

//import org.junit.Test;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import main.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = Application.class)

public class UserConfigurationTest {
	
	
	@Test
	public void bootstrapApplication() {
		new AnnotationConfigApplicationContext(Application.class);
	}

}
