package repository;

import static org.fest.assertions.Assertions.assertThat;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;


//import com.deswaef.spring.examples.datajpa.UserConfiguration;
//import org.junit.runner.RunWith;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.IntegrationTest;
//import org.springspringframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//@SpringApplicationConfiguration(classes = Application.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@IntegrationTest("server.port:0")
//@Transactional
//



public class ProduitRepositoryTest {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(productRepository.findAll()).hasSize(1);
	}
	
	@Test
	public void findByNomAndPrix_andNoneFound() {
		assertThat(productRepository.findByNameAndPrice("android",10)).isNull();
	}
	
	@Test
	public void findByNomAndPrix_andFoundOne() {
		assertThat(productRepository.findByNameAndPrice("iPhone" , 10)).isNotNull();

}

}	