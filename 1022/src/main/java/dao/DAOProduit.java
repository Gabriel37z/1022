package dao;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Planete;
import util.Context;

public class DAOProduit {
	
	public void insert(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
 
        em.persist(p);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public Produit selectById(int id) {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Produit p = em.find(Produit.class, id);
        System.out.println(p);
        em.close();
        Context.destroy();
		return p;
	}
	
	public List<Produit> selectAll() {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Query query =  em.createQuery("from Planete");
        List<Produit> Produits = query.getResultList();
       
        em.close();
        Context.destroy();
		return Produits;
	}
	
	public void update(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
        
        em.merge(p);

        //em.persist(a);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public void delete(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();

        p=em.merge(p);
        em.remove(p);

        //em.persist(p);
   
        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
}


}
