package model;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Product")
public class Produit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	
	@Column(name="name", nullable=false)
	private String nom;
	
	@Column(name="prix")
	private int prix;
	
	public Produit(String nom, int prix) {
		this.nom = nom;
		this.prix = prix;
	}

	public Produit() {
		
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", prix=" + prix + "]";
	}
	
	

}
