package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import service.ProduitService;
import model.Produit;

@Component
public class ProduitProducer {
	private Log logger = LogFactory.getLog(ProduitProducer.class);
	private ProduitService productService;


	@Autowired
	private ProduitProducer(ProduitService productService) {
		this.productService = productService;
	}

	@PostConstruct
	public void produceData()  {
		findProducts();
		addOneProduct();
		findProducts();
	}

	private void addOneProduct() {
		logger.info("-> Adding new product now!");
		productService.addProduit(new Produit("iPhone", 10));
	}

	private void findProducts() {
		logger.info("Trying to find all products.");
		List<Produit> allProducts = productService.getAllProduits();
		if(allProducts.isEmpty()) {
		}else {
			for (Produit foundProduct : allProducts) {

				logger.info(String.format("product wich id %d and name %s found :)",
						foundProduct.getId(), foundProduct.getNom()));
			}
		}

	}
}
	


