package service;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import model.Produit;

import java.util.List;

import repository.*;




@Service
@Transactional
public class ProduitService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Transactional(readOnly = true)
	public List<Produit> getAllProduits() {
		return productRepository.findAll();
	}
	
	public void addProduit(Produit produit) {
		
		productRepository.save(produit);
	}
	
	
	
	
	
	

}
